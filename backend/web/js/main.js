$(function(){
	//get the click event
	$('#modalButton').click(function (){
		$('#modal').modal('show')
			.find('#modalContents')
			.load($(this).attr('value'));
	});
});