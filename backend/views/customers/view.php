<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use backend\models\Customers;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Customers */

$this->title = $model->customer_name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vendorsVendor.vendor_name',
            'customer_name',
            'customer_address',
            'customer_gender',
            'productsProduct.product_name',
        ],

    ]) ?>

      <p>
        <?= Html::a('Update', ['update', 'id' => $model->customer_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->customer_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php Pjax::begin(); ?>
     <?= GridView::widget([
     'dataProvider' => $dataProvider,
     'filterModel' => $searchModel,
     'columns' => [
         ['class' => 'yii\grid\SerialColumn'],
         'vendorsVendor.vendor_name',
            'customer_name',
            'customer_address',
            'customer_gender',
         [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {update} {delete} {lists}',
        'buttons' => [
                'update' => function ($url,$model) {
                        return Html::a(
                                '<span class="glyphicon glyphicon-user"></span>', 
                                $url);
                },
                'lists' => function ($url,$model,$key) {
                                return Html::a('Action', $url);
                    },
                ],
            ],
        ],
        ]); ?>
     <?php Pjax::end(); ?>

</div>
