<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property integer $customer_id
 * @property integer $vendors_vendor_id
 * @property string $customer_name
 * @property string $customer_address
 * @property string $customer_gender
 * @property integer $products_product_id
 *
 * @property Vendors $vendorsVendor
 * @property Products $productsProduct
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendors_vendor_id', 'customer_name', 'customer_address', 'customer_gender', 'products_product_id'], 'required'],
            [['vendors_vendor_id', 'products_product_id'], 'integer'],
            [['customer_gender'], 'string'],
            [['customer_name'], 'string', 'max' => 100],
            [['customer_address'], 'string', 'max' => 255],
            [['vendors_vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vendors::className(), 'targetAttribute' => ['vendors_vendor_id' => 'vendor_id']],
            [['products_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['products_product_id' => 'product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'vendors_vendor_id' => 'Vendors Name',
            'customer_name' => 'Name',
            'customer_address' => 'Address',
            'customer_gender' => 'Gender',
            'products_product_id' => 'Products Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorsVendor()
    {
        return $this->hasOne(Vendors::className(), ['vendor_id' => 'vendors_vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsProduct()
    {
        return $this->hasOne(Products::className(), ['product_id' => 'products_product_id']);
    }
}
