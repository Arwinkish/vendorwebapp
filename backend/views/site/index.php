<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Sales App Beta</h1>

        <p class="lead">Welcome.</p>

        <p><a class="btn btn-lg btn-success" href="http://localhost/advanced/backend/web/index.php?r=customers">Get started</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Create Vendors</h2>

                <p>Create's a new vendor. Only a user with a user level of admin can create vendors. If a user with lower credentials clicks on it, he will get a 404 error.</p>

                <p><a class="btn btn-default" href="http://localhost/advanced/backend/web/index.php?r=vendors">Create vendor &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Create products</h2>

                <p>Create's a new product. There must be at least one company for this to be possible. Only a user with a user level of admin can create product. If a user with lower credentials clicks on it, he will get a 404 error</p>

                <p><a class="btn btn-default" href="http://localhost/advanced/backend/web/index.php?r=products">Create products &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Add customers</h2>

                <p>Create's a new customer. There must be at least one company and product for this to be possible. Only a user with a user level of admin can create customers. If a user with lower credentials clicks on it, he will get a 404 error</p>

                <p><a class="btn btn-default" href="http://localhost/advanced/backend/web/index.php?r=customers">Add a customer &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
