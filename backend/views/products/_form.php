<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Vendors;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vendors_vendor_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Vendors::find()->all(), 'vendor_id','vendor_name'),
    'language' => 'en',
    'options' => ['placeholder' => 'Select a vendor'],
    'pluginOptions' => [
        'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
