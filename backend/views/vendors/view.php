<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Vendors */

$this->title = $model->vendor_name;
$this->params['breadcrumbs'][] = ['label' => 'Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendors-view col-sm-9">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="center-block">
        <img class="col-xs-8 col-sm-6 img-thumbnail" src="<?php echo $model->vendor_logo;?>">
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vendor_id',
            'vendor_name',
            'vendor_email:email',
            'vendor_address',
        ],
    ]) ?>

     <p>
        <?= Html::a('Update', ['update', 'id' => $model->vendor_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->vendor_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
