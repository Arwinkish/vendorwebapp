<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $product_id
 * @property string $product_name
 * @property integer $vendors_vendor_id
 *
 * @property Customers[] $customers
 * @property Vendors $vendorsVendor
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name', 'vendors_vendor_id'], 'required'],
            [['vendors_vendor_id'], 'integer'],
            [['product_name'], 'string', 'max' => 100],
            [['vendors_vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vendors::className(), 'targetAttribute' => ['vendors_vendor_id' => 'vendor_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_name' => 'Product Name',
            'vendors_vendor_id' => 'Vendors Vendor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['products_product_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorsVendor()
    {
        return $this->hasOne(Vendors::className(), ['vendor_id' => 'vendors_vendor_id']);
    }
}
