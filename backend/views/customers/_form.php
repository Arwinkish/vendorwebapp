<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Vendors;
use backend\models\Products;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customers-form">

   <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_gender')->dropDownList([ 'male' => 'Male', 'female' => 'Female', ], ['prompt' => '']) ?>


   <?= $form->field($model, 'vendors_vendor_id')->dropDownList(
   								ArrayHelper::map( Vendors::find()->all(), 'vendor_id','vendor_name'),
   								[
   									'prompt' => 'Select vendor',
   									'onchange'=>'
   										$.post("index.php?r=customers/lists&id='.'"+$(this).val(),function(data){
   											$( "select#customers-products_product_id").html( data );
   										});'

   								]);
    ?>

    <?= $form->field($model, 'products_product_id')->dropDownList(
   								ArrayHelper::map( Products::find()->all(), 'product_id','product_name'),
   								[
   									'prompt' => 'Select product',

   								]);
    ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
