<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use backend\models\Customers;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Create Customers', ['value'=>Url::to('index.php?r=customers/create'),
        'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
    <?php
            Modal::begin([
                    'header'=>'<h4>New</h4>',
                    'id'=>'modal',
                    'size' =>'modal-lg',
                ]);
            echo "<div id='modalContents'></div>";

            Modal::end();
    ?>

    <?php Pjax::begin(); ?>
     <?= GridView::widget([
     'dataProvider' => $dataProvider,
     'filterModel' => $searchModel,
     'columns' => [
         ['class' => 'yii\grid\SerialColumn'],
         'vendorsVendor.vendor_name',
            'customer_name',
            'customer_address',
            'customer_gender',
         [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {update} {delete} {lists}',
        'buttons' => [
                'update' => function ($url,$model) {
                        return Html::a(
                                '<span class="glyphicon glyphicon-user"></span>', 
                                $url);
                },
                'lists' => function ($url,$model,$key) {
                                return Html::a('Action', $url);
                    },
                ],
            ],
        ],
        ]); ?>
     <?php Pjax::end(); ?>
    
</div>
