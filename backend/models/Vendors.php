<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vendors".
 *
 * @property integer $vendor_id
 * @property string $vendor_name
 * @property string $vendor_email
 * @property string $vendor_address
 *
 * @property Customers[] $customers
 * @property Products[] $products
 */
class Vendors extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_name', 'vendor_email', 'vendor_address'], 'required'],
            [['vendor_name', 'vendor_email'], 'string', 'max' => 100],
            [['file'], 'file'],
            [['vendor_address', 'vendor_logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => 'Vendor ID',
            'vendor_name' => 'Vendor Name',
            'vendor_email' => 'Vendor Email',
            'vendor_address' => 'Vendor Address',
            'file' => 'Logo'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['vendors_vendor_id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['vendors_vendor_id' => 'vendor_id']);
    }
}
